$( document ).ready(function() {
    $.ajax({
        url: "https://kazys-cernauskis.gitlab.io/covid-19-lt/geo.json",
        success: success,
        dataType: "json"
      });
})

function success(data){
    drawMapStats(data)
    drawTableArea(data)
}

function drawMapStats(data){
    for (let key in data) {
        $('#'+key+" text").text(data[key]['cnt'])
        $('#'+key+" circle").attr("r",20+(Math.log10(data[key]['cnt'])*5))
        console.log(key, data[key]);
    }
}

function drawTableArea(data){
    for (let key in data) {
        console.log(key, data[key]);
        $('#table_stats_area').append('<tr scope="row"><td>'+data[key]['name']+'</td><td>'+data[key]['cnt']+'</td></tr>');
      }
}

