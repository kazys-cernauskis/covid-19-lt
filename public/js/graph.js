covid_data = {labels:[],infect:[],infect_a:[],infect_t:0,recover:[],recover_a:[],recover_t:0,dead:[],dead_a:[],dead_t:0}

var config_daily = {
  type: 'bar',
  data: {
    labels: covid_data['labels'],
    datasets: [{
      backgroundColor:"rgba(250, 140, 0, 0.1)",
      borderColor:"rgba(250, 140, 0, 1)",
      borderWidth:1,
      label: "Infected",
      data: covid_data['infect']
    },
    {
      backgroundColor:"rgba(50,205,50, 0.1)",
      borderColor:"rgba(50,205,50, 1)",
      borderWidth:1,
      label: "Recovered",
      data: covid_data['recover']
    },
    {
      backgroundColor:"rgba(0, 0, 0, 0.1)",
      borderColor:"rgba(0, 0, 0, 1)",
      borderWidth:1,
      label: "Deaths",
      data: covid_data['dead']
    }
  ]
  },
  options: {
    scales: {
        yAxes: [{
            ticks: {
                stepSize:2
            }
        }],
      xAxes: [{
        type: 'time',
        offset: true,
        ticks:{
            maxRotation: 45,
            minRotation: 45
        },
        time: {
          unit: 'day',
          displayFormats: {
            'day': 'MM-DD',
            'week': 'MM-DD',
            'month': 'MM-DD',
            'quarter': 'MM-DD',
            'year': 'MM-DD'
          }
        }
      }],
    },
  }
};

var config_actual = {
  type: 'line',
  data: {
    labels: covid_data['labels'],
    datasets: [{
      backgroundColor:"rgba(250, 140, 0, 0.1)",
      borderColor:"rgba(250, 140, 0, 1)",
      borderWidth:1,
      label: "Infected",
      data: covid_data['infect_a']
    },
    {
      backgroundColor:"rgba(50,205,50, 0.1)",
      borderColor:"rgba(50,205,50, 1)",
      borderWidth:1,
      label: "Recovered",
      data: covid_data['recover_a']
    },
    {
      backgroundColor:"rgba(0, 0, 0, 0.1)",
      borderColor:"rgba(0, 0, 0, 1)",
      borderWidth:1,
      label: "Deaths",
      data: covid_data['dead_a']
    }
  ]
  },
  options: {
    legend: {
        display: false
    },
    scales: {
      xAxes: [{
        type: 'time',
        offset: true,
        ticks:{
            maxRotation: 45,
            minRotation: 45
        },
        time: {
          unit: 'day',
          displayFormats: {
            'day': 'MM-DD',
            'week': 'MM-DD',
            'month': 'MM-DD',
            'quarter': 'MM-DD',
            'year': 'MM-DD'
          }
        }
      }],
    },
  }
};

function drawGraphs(){
  var ctx1 = document.getElementById("timelineGraph").getContext("2d");
  new Chart(ctx1, config_daily);
  var ctx2 = document.getElementById("timelineGraph2").getContext("2d");
  new Chart(ctx2, config_actual);
}


$( document ).ready(function() {
  $.ajax({
    url: "https://kazys-cernauskis.gitlab.io/covid-19-lt/timeline.json",
    success: drawData,
    dataType: "json"
  });
  
});

function drawTable(data){
  for (let key in data) {
    console.log(key, data[key]);
    $('#table_stats').prepend('<tr scope="row"><td>'+key.substring(5)+'</td><td>'+data[key]['infected']+'</td><td>'+data[key]['dead']+'</td><td>'+data[key]['recover']+'</td></tr>');
  }
}

function drawCaunter(){
  $('#infect_couter').text(covid_data['infect_t'])
  $('#dead_couter').text(covid_data['dead_t'])
  $('#recover_couter').text(covid_data['recover_t'])
}

function drawData(data, textStatus, jqXHR){
  drawTable(data)
  var i = 0;
  for (let key in data) {
    console.log(key, data[key]);
    covid_data['labels'].push(moment(key))
    covid_data['infect'].push(data[key]['infected'])
    covid_data['recover'].push(data[key]['recover'])
    covid_data['dead'].push(data[key]['dead'])
    covid_data['infect_t'] += data[key]['infected']
    covid_data['recover_t'] += data[key]['recover']
    covid_data['dead_t'] += data[key]['dead']
    if(i > 0){
      covid_data['infect_a'][i] = covid_data['infect_a'][i-1] + covid_data['infect'][i]
      covid_data['recover_a'][i] = covid_data['recover_a'][i-1] + covid_data['recover'][i]
      covid_data['dead_a'][i] = covid_data['dead_a'][i-1] + covid_data['dead'][i]
    }
    else{
      covid_data['infect_a'][i] = covid_data['infect'][i]
      covid_data['recover_a'][i] = covid_data['recover'][i]
      covid_data['dead_a'][i] = covid_data['dead'][i]
    }
    i++;
  }
  console.log(covid_data)
  drawGraphs()
  drawCaunter()
  drawTable()
}
